<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/



$router->post('api/login', ['uses' => 'Api\AuthController@login']);
$router->post('api/register', ['uses' => 'Api\AuthController@register']);

$router->post('api/resetpassword/sendotp', 'Api\PasswordController@sendOtpEmail');
$router->post('api/password/reset', 'Api\PasswordController@reset');

$router->group(['middleware' => ['jwt.verify']], function () use ($router) {
    $router->get('whoami', ['uses' => 'Api\AuthController@whoami']);
    $router->get('api/logout', ['uses' => 'Api\AuthController@logout']);
    $router->get('api/refreshtoken', ['uses' => 'Api\AuthController@refreshtoken']);
});