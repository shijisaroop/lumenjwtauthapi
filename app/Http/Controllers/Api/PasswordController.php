<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\PasswordReset;
use Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendOtpResetPassword;
/**
 * PasswordController handle password reset
 * Athor: Shiji
 */
class PasswordController extends Controller
{
    
    /**
     * API forgot password
     * Send OTP  to the given user
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendOtpEmail(Request $request)
    {
        $oValidator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255|exists:users,email'
        ]);

        if($oValidator->fails()){
            return response()->json([
                'success'=> false, 
                'message' => "Invalid user",
                'error'=> $oValidator->messages()], 400);
        }

        try {
            $email = $request->get('email');           
            $verification_code = str_random(8); //Generate verification code
            $passwordReset = PasswordReset::updateOrCreate(
                ['email' => $email],
                [
                    'email' => $email,
                    'token' => $verification_code,
                    'created_at' => date("Y-m-d H:i:s")
                 ]
            );
            
            $oUser = User::where("email", $email)->first();
            $oResetOTP = PasswordReset::find($email);
            Mail::to($email)
                ->send(new SendOtpResetPassword($oUser, $oResetOTP));
    
            return response()->json( [
				'status' => true,
				'message' => 'Please check your email and use the OTP to reset password'
            ], 201);
            
        } catch (\Exception $e) {
			return response()->json([
				'success' => false,
				'message' => 'OTP creation or sending failed',
				'exception' => $e->getMessage()
			], 500);
		}
        
    }

    /**
     * API forgot password reset
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function reset(Request $request)
    {
        $oValidator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255|exists:password_resets,email',            
            'password' => 'required|string|min:6|confirmed',
            'otp' => 'required|string'
        ]);

        if($oValidator->fails()){
            return response()->json([
                'success'=> false, 
                'message' => "Invalid credentials",
                'error'=> $oValidator->messages()], 400);
        }

        try {
            $email = $request->get('email'); 
            $otp = $request->get('otp'); 
            //Check expiry date          
            $expiry_minutes = 15;
            $time = new \DateTime();
            $time->sub(new \DateInterval('PT' . $expiry_minutes . 'M'));

            $checkExpiryDate = $time->format('Y-m-d H:i:s');
            $oPasswordReset = PasswordReset::where('token', $otp) 
                                            ->where('email', $email)
                                            ->where('created_at','>', $checkExpiryDate);
            if (!empty($oPasswordReset->first())) {
                $oUser = User::where("email", $email)->update(['password'   => Hash::make($request->get('password'))]);
                $oResetOTP = $oPasswordReset->delete();

                return response()->json([
                    'success' => true,
                    'message' => 'Please login using the new password'
                ], 400);
            } else {
                $oResetOTP = $oPasswordReset->delete();
                return response()->json([
                    'success' => false,
                    'message' => 'OTP Expired'
                ], 400);
            }                                            
            
            
        } catch (\Exception $e) {
			return response()->json([
				'success' => false,
				'message' => 'Reset password failed',
				'exception' => $e->getMessage()
			], 500);
		}
        
    }

}