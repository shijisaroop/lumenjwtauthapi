<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use Validator;
/**
 * AuthController handle login/register and generate jwt token
 * Athor: Shiji
 */
class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth:api', ['except' => ['login','register']]);
    }

    /**
     * API Login
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $oValidator = Validator::make($request->all(), [
			'email' => 'required|string|email|max:255,email',
			'password' => 'required|string|min:6',
		]);
		
		if ($oValidator->fails()) {
			return response()->json([
				'success' => false,
				'message' => 'Validation errors.',
				'errors' => $oValidator->errors()
			], 400);
		}
        
        if (! $token = Auth::attempt($request->only(['email', 'password']))) {
            return response()->json(['success' => false, 'message' => 'Unauthorized'], 401);
        }
        return $this->respondWithToken($token);
    }
    /**
     * API Register
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $oValidator = Validator::make($request->all(), [
            'firstname' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email',
            'password' => 'required|string|min:6|confirmed'
        ]);

        if($oValidator->fails()){
            return response()->json([
                'success'=> false,                 
                'message' => 'Validation errors.',
                'error'=> $oValidator->messages()], 400);
        }

        try {
            $oUser = User::create([
                'firstname' => $request->get('firstname'),
                'lastname' => $request->get('lastname'),
                'phone' => $request->get('phone'),
                'email' => $request->get('email'),
                'password' => Hash::make($request->get('password')),
                'is_avtive' => 1
            ]);
    
            return response()->json( [
				'status' => 'success',
				'message' => 'Your account is created successfully.'
            ], 201);
            
        } catch (\Exception $e) {
			return response()->json([
				'success' => false,
				'message' => 'Internal Server Error.',
				'exception' => $e->getMessage()
			], 500);
		}
        
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function whoami(Request $request)
    {
        return response()->json(['success' => true,
                                 'user' => Auth::guard('api')->user()]);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        Auth::guard('api')->logout();
        return response()->json([
            'status' => true,
            'message' => 'Successfully logged out'
        ], 200);
        
    }
    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refreshtoken()
    {
        return $this->respondWithToken(Auth::refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => Auth::factory()->getTTL() * 60
        ]);
    }    

}