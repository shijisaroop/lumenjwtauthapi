# Lumen API with JWT Authentication
Basically this is a starter kit for you to integrate Lumen with [JWT Authentication](https://jwt.io/).

## What's Added

- [Lumen 5.8](https://github.com/laravel/lumen/tree/v5.8.0).
- [JWT Auth](https://github.com/tymondesigns/jwt-auth) for Lumen Application.
 
## Quick Start

- Clone this repo or download it's release archive and extract it somewhere
- You may delete `.git` folder if you get this code via `git clone`
- Run `composer install`
- Run `php artisan jwt:secret`
- Configure your `.env` file for authenticating via database (Copy ".env.example" to ".env" and configure it)
- Set the `API_PREFIX` parameter in your .env file (usually `api`).
- Create MySql database and Run `php artisan migrate` . 

`*  Note: Before running migration , if you want any changes to the user table, please include that in migration file`

## A Live PoC

- Run a PHP built in server from your root project:

```sh
php -S localhost:8000 -t public/
```

Or via artisan command:

```sh
php artisan serve
```
To register a user, make a `POST` request to `/api/register` with parameter as mentioned below:

```
firstname:Test
lastname:User
email:your@email.com
phone : 1234567
password:password_string
password_confirmation:password_string
```

Response
```
{
    "status": "success",
    "message": "Your account is created successfully."
}
```

To authenticate a user, make a `POST` request to `/api/login` with parameter as mentioned below:

```
email: johndoe@example.com
password: johndoe
```

Request:

```sh
curl -X POST -F "email=johndoe@example.com" -F "password=johndoe" "http://localhost:8000/api/auth/login"
```

Response:

```
{
    "access_token": "ACCESS_TOKEN_APEARS_HERE",
    "token_type": "bearer",
    "expires_in": 3600
}
```

#Reset Password
To reset password of a user, make a `POST` request to `api/resetpassword/sendotp` with parameter as mentioned below. You will recieve an OTP to reset password in email:
`Note: Make sure you have set the email configuration in .env`

```
email: johndoe@example.com
```

Request:

```sh
curl -X POST -F "email=johndoe@example.com"  "http://localhost:8000/api/resetpassword/sendotp"
```

Response:

```
{
    "success": true,
    "message": "Check your email for OTP to reset password"
}
```
- With token provided by above request, you can check authenticated user by sending a `GET` request to: `/whoami`.

Request:

```sh
curl -X GET -H "Authorization: Bearer PUT_ACCESS_TOKEN__HERE" "http://localhost:8000/whoami"
```

Response:

```
{
    "success": true,
    "user": {
        "id": 2,
        "firstname": "Test",
        "lastname": "user",
        "email": "hello@test.com",
        "phone": '12345567',
        "is_active": "1",
        "created_at": "2019-03-31 17:27:31",
        "updated_at": "2019-03-31 17:27:31"
    }
}
```

- To refresh your token, simply send a `POST` request to `/api/refreshtoken` with Authorization header with bearer token.
- To Logout and invalidate token by sending a `GET` request to `/api/logout`.

```
⇒  php artisan route:list
+------+----------------------------+------------+---------------------------------------------+--------------+------------+
| Verb | Path                       | NamedRoute | Controller                                  | Action       | Middleware |
+------+----------------------------+------------+---------------------------------------------+--------------+------------+
| GET  | /                          |            | None                                        | Closure      |            |
| POST | /api/login                 |            | App\Http\Controllers\Api\AuthController     | login        |            |
| POST | /api/register              |            | App\Http\Controllers\Api\AuthController     | register     |            |
| POST | /api/resetpassword/sendotp |            | App\Http\Controllers\Api\PasswordController | sendOtpEmail |            |
| POST | /api/password/reset        |            | App\Http\Controllers\Api\PasswordController | reset        |            |
| GET  | /whoami                    |            | App\Http\Controllers\Api\AuthController     | whoami       | jwt.verify |
| GET  | /api/logout                |            | App\Http\Controllers\Api\AuthController     | logout       | jwt.verify |
| GET  | /api/refreshtoken          |            | App\Http\Controllers\Api\AuthController     | refreshtoken | jwt.verify |
+------+----------------------------+------------+---------------------------------------------+--------------+------------+
```
`Note:` Genereated JWT token need to be passed in request header as bearer token. You can save this token in localstorage and reuse whenever required in authorization header.
`Refer this for more info:` [JWT Token storage Options](https://stormpath.com/blog/where-to-store-your-jwts-cookies-vs-html5-web-storage#json-web-tokens-jwt-a-crash-course)

